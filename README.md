
# lcp (logcat parser) is a small tool for parsing logcat files

## simple to install

```bash
pip install -i https://test.pypi.org/simple/ lcp
```  
  
  
But you can also use it as it is, just download or copy the lcp.py file.

---

## Dependencies
lcp is written with python3.6 and i recommend using [virtualenv](https://pypi.org/project/virtualenv/) with it.

lcp depends on [argparse](https://docs.python.org/3.6/library/argparse.html#module-argparse) module, it will be installed automatically

---

## Usage example
### Once installed with pip you can use it as:


```bash
python3 -m lcp logcat_file.txt -i test
```
or 
```bash
python3 -m lcp logcat_file.txt -s
```
or with multiple arguments
```bash
python3 -m lcp logcat_file.txt -i alert test
```  
  
### If you just coppied or downloaded the lcp.py
  
  navigate to downloads folder, open terminal in it and use with:

```bash  
python3 lcp.py logcat_file.txt -s
```
  
---

## More info

You can find it also at [pypi test](https://test.pypi.org/project/lcp/)

The [demo file](https://bitbucket.org/onePunchBakka/lcp/src/master/logcat_file.txt) for trying the tool out.