"""
Task:
        Develop a tool in python with the following swtiches:
                -h prints out help containing info about all available
                switches

                -s prints out the time difference between lines containing
                "TEST STARTED" and "TEST FINISHED"

                -i <args,...> prints out lines containing all arguments

                -e <args,...> prints out lines wich dont't contain any
                of the provided arguments. (EXCLUSION)

        Usage example:
                python lcp.py logcat_file.txt -i word1,word2,word3


Author:
        Mario Radomirovic
"""
import argparse
from datetime import datetime as dt

def text_parser_object(self, filename):
        with open(filename, 'r') as f:
                read_data = f.readlines()
        return  read_data
# #write to file factory
# def write_to_file(self, output, data):
#         with open(output, 'w') as o:
#                 o.write(line)
#         pass

def main():
        ### Instantiate parser
        parser = argparse.ArgumentParser(prog='lcp.py',
        description='Tool for parsing logcat files')
        ## Parser arguments start
        parser.add_argument('filename', type=str, help='Name of the file you want to parse')
        parser.add_argument('-s', dest='started', action='store_true',
                help='''Prints out the time difference between lines containing
                "TEST STARTED" and "TEST FINISHED"
                ''')
        parser.add_argument('-i', dest='include', type=str, nargs='*',
                help='Prints out lines containing all arguments')
        parser.add_argument('-e', dest='exclude', type=str, nargs='*',
                help='Prints out lines wich dont contain any of the provided arguments')
        ## todo parser for file output
        # parser.add_argument('-o', dest='output', type=str,
        #         help='Outputs the parsed info to a file')
        ### Parser arguments end
        
        ## Instantiate parser arguments
        args = parser.parse_args()

        filename = args.filename
        ## instantiate text_parser_object
        new_parser = text_parser_object(object, filename)
        ## todo: add output to a file
        # new_file = write_to_file(object, filename, data)
        ## time formatting
        fmt = '%m-%d %H:%M:%S'
        
        ### Logic start
        if filename and args.include:
                for arg in args.include:
                        for line in new_parser:
                                if arg in line:
                                        print(line)
        elif filename and args.exclude:
                for arg in args.exclude:
                        for line in new_parser:
                                if arg not in line:
                                        print(line)
        elif filename and args.started:
                for line in new_parser:
                        if 'STARTED' in line:
                                started = line[0:14]
                        elif 'FINISHED' in line:
                                finished = line[0:14]
                #Extracting the time from the string using the fmt time format
                print('The test started at --> {}'.format(started))
                print('The test finished at --> {}'.format(finished))
                difference = dt.strptime(finished, fmt) - dt.strptime(started, fmt)
                print('The test took ', difference, 'to complete.')
        else:
                print('Please provide an argument [-i, -e, -s] in order to use')
        ### Logic end

if __name__ == '__main__':
        main()
        