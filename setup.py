import setuptools

with open('readme.md', 'r') as f:
	long_description = f.read()

setuptools.setup(
	name='lcp',
	version='0.0.02',
	author='Mario Radomirovic',
	author_email='reignofcolours@gmail.com',
	decription='A small tool for parsing logcat files',
	long_description=long_description,
	long_description_content_type='text/markdown',
	url='https://bitbucket.org/onePunchBakka/lcp',
	packages=setuptools.find_packages(),
	license= 'MIT',
	classifiers=[
	'Programming Language :: Python :: 3.6',
	'License :: OSI Approved :: MIT License'],
	entry_points={
		'console_scripts':[
		'lcp=lcp:main',
		]
	},)
